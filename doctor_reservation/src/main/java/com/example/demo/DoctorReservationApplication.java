package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.dao.AppointmentRepo;
import com.example.demo.dao.DoctorRepo;
import com.example.demo.dao.PatientRepo;
import com.example.demo.dao.WorkTimeRepo;
import com.example.demo.domain.Appointment;
import com.example.demo.domain.Doctor;
import com.example.demo.domain.Patient;
import com.example.demo.domain.WorkTime;

@SpringBootApplication
public class DoctorReservationApplication implements ApplicationRunner {
	@Autowired
	DoctorRepo doctorRepo;
	@Autowired
	PatientRepo patientRepo;
	@Autowired
	AppointmentRepo appointmentRepo;
	@Autowired
	WorkTimeRepo workTimeRepo;

	public static void main(String[] args) {
		SpringApplication.run(DoctorReservationApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// dummy doctor
		Doctor doctor1 = new Doctor("ahmed abdelmagied", "opthalmology");
		Doctor doctor2 = new Doctor("salama salah", "pediatrics");
		Doctor doctor3 = new Doctor("ahmed salah eldin mohamed", "Ear,nose and throad");
		Doctor doctor4 = new Doctor("mona mohamed", "Internal medecine");
		Doctor doctor5 = new Doctor("ahmed salah zaki", "opthalmology");
		Doctor doctor6 = new Doctor("zaki abotreika", "pediatrics");
	 
		Doctor doctor8 = new Doctor("mohamed salah", "Internal medecine");
		Doctor doctor9 = new Doctor("ahmed mohamed eraky", "opthalmology");
		Doctor doctor10 = new Doctor("mona abdelaziz", "pediatrics");

		doctorRepo.save(doctor1);
		doctorRepo.save(doctor2);
		doctorRepo.save(doctor3);
		doctorRepo.save(doctor4);
		doctorRepo.save(doctor5);
		doctorRepo.save(doctor6);
		doctorRepo.save(doctor8);
		doctorRepo.save(doctor9);
		doctorRepo.save(doctor10);

		Patient patient1 = new Patient("moahmed", "male");
		Patient patient2 = new Patient("mona", "female");
		Patient patient3 = new Patient("sahar", "female");
		Patient patient4 = new Patient("salama", "male");
		Patient patient5 = new Patient("sayed ahmed", "male");
		Patient patient6 = new Patient("mona tarek", "female");
		Patient patient7 = new Patient("abdelmageid", "male");
		Patient patient8 = new Patient("hussein", "male");
		Patient patient9 = new Patient("moahmed", "male");
		Patient patient10 = new Patient("gad", "male");
		patientRepo.save(patient1);
		patientRepo.save(patient2);
		patientRepo.save(patient3);
		patientRepo.save(patient4);
		patientRepo.save(patient5);
		patientRepo.save(patient6);
		patientRepo.save(patient7);
		patientRepo.save(patient8);
		patientRepo.save(patient9);
		patientRepo.save(patient10);

		Appointment appointment1 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "saturday 2019-01-15 09:01:15",doctor1, patient1);
//
		Appointment appointment2 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "sunday 2019-01-16 09:01:15",doctor2 , patient2);
		Appointment appointment3 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "monday 2019-01-17 09:01:15",doctor3 , patient3);
		Appointment appointment4 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "tuesday 2019-01-18 09:01:15",doctor4 , patient4);
		Appointment appointment5 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", "thrusday 2019-01-18 09:01:15",doctor6 , patient6);
		Appointment appointment7 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "saturday 2019-01-12 09:01:15",doctor8 , patient8);
		Appointment appointment8 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", "sunday 2019-01-14 09:01:15",doctor9 , patient9);
		Appointment appointment9 = new Appointment("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", "sunday 2019-01-11 09:01:15",doctor10 , patient10);
		appointmentRepo.save(appointment1);
	appointmentRepo.save(appointment2);
		appointmentRepo.save(appointment3);
		appointmentRepo.save(appointment4);
		appointmentRepo.save(appointment5);
		appointmentRepo.save(appointment7);
		appointmentRepo.save(appointment8);
		appointmentRepo.save(appointment9);

		WorkTime worktime1 = new WorkTime("saturday", "2019-01-15 09:01:15", "2019-01-15 11:01:15");
		WorkTime worktime2 = new WorkTime("sunday", "2019-01-15 09:01:15", "2019-01-15 11:01:15");
		WorkTime worktime3 = new WorkTime("monday", "2019-01-15 09:01:15", "2019-01-15 11:01:15");
		WorkTime worktime4 = new WorkTime("thursday", "2019-01-15 09:01:15", "2019-01-15 11:01:15");
		Set<Doctor> doctors = new HashSet<Doctor>();
		int[] ids = {1,6,9};
		for(int i = 0 ; i < ids.length ; i++) {
			doctors.add(doctorRepo.findById(ids[i]).orElse(null));
		}
		
		Set<Doctor> doctors1 = new HashSet<Doctor>();
		int[] ids1 = {1,2,7};
		for(int i = 0 ; i < ids1.length ; i++) {
			doctors1.add(doctorRepo.findById(ids1[i]).orElse(null));
		}
		Set<Doctor> doctors2 = new HashSet<Doctor>();
		int[] ids2 = {1,3,8};
		for(int i = 0 ; i < ids2.length ; i++) {
			doctors2.add(doctorRepo.findById(ids2[i]).orElse(null));
		}
		Set<Doctor> doctors3 = new HashSet<Doctor>();
		int[] ids3 = {1,4,9};
		for(int i = 0 ; i < ids3.length ; i++) {
			doctors3.add(doctorRepo.findById(ids3[i]).orElse(null));
		}
		worktime1.setDoctors(doctors);
		worktime2.setDoctors(doctors1);
		worktime3.setDoctors(doctors2);
		worktime4.setDoctors(doctors3);
		workTimeRepo.save(worktime1);
		
		workTimeRepo.save(worktime2);
		workTimeRepo.save(worktime3);
		workTimeRepo.save(worktime4);
		

	}

}
