package com.example.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int patientId;
	@NotEmpty(message = "name must not be empty")
	String name;
	@NotEmpty(message = "gender must not be empty")
	String gender;

	@ManyToOne
	@JoinColumn(name = "doctor_id")
	Doctor doctor;

	@OneToOne()
	@JoinColumn(name = "appointment_id")
	Appointment appointment;

	public Patient() {
	}

	public Patient(String name, String gender) {
		super();
		this.name = name;
		this.gender = gender;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

}
