package com.example.demo.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class WorkTime {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int workTimeId;
	String day;
	String dtartDate;
	String endDate;

	@ManyToMany
	@JoinTable(name = "Doctor_WorkTime", joinColumns = @JoinColumn(name = "workTime_id"), inverseJoinColumns = @JoinColumn(name = "doctor_id"))
	Set<Doctor> doctors = new HashSet<>();

	public WorkTime() {
	}

	public WorkTime(String day, String dtartDate, String endDate) {
		super();
		this.day = day;
		this.dtartDate = dtartDate;
		this.endDate = endDate;
	}

	public Set<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}

	public int getWorkTimeId() {
		return workTimeId;
	}

	public void setWorkTimeId(int workTimeId) {
		this.workTimeId = workTimeId;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDtartDate() {
		return dtartDate;
	}

	public void setDtartDate(String dtartDate) {
		this.dtartDate = dtartDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
