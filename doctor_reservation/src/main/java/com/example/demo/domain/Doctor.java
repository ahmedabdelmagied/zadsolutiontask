package com.example.demo.domain;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

@Entity
@Table(name = "doctor")
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int doctorId;
	@NotEmpty(message = "name must not be empty")
	String name;
	@NotEmpty(message = "specialize must not be empty")
	String specialize;

	@OneToMany(mappedBy = "doctor")
	Set<Patient> patients = new HashSet<>();

	@OneToMany(mappedBy = "doctor")
	Set<Appointment> appointments = new HashSet<>();

	@ManyToMany(mappedBy = "doctors")
	Set<WorkTime> worktimes = new HashSet<>();

	public Doctor() {
	}

	public Doctor(String name, String specialize) {
		super();
		this.name = name;
		this.specialize = specialize;
	}

	public Set<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}

	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Set<WorkTime> getWorktimes() {
		return worktimes;
	}

	public void setWorktimes(Set<WorkTime> worktimes) {
		this.worktimes = worktimes;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialize() {
		return specialize;
	}

	public void setSpecialize(String specialize) {
		this.specialize = specialize;
	}

}
