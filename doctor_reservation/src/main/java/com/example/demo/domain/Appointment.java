package com.example.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
public class Appointment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int appointmentId;
  
	String breifComplain;
	@NotEmpty(message = "appointment time must not be empty")
	String appintmentTime;
	@ManyToOne
	@JoinColumn(name = "doctor_id")
	Doctor doctor;
	@OneToOne
	@JoinColumn(name = "patient_id")
	Patient patient;

	public Appointment() {
	}

	public Appointment(String breifComplain, String appintmentTime, Doctor doctor, Patient patient) {

		this.breifComplain = breifComplain;
		this.appintmentTime = appintmentTime;
		this.doctor = doctor;
		this.patient = patient;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public int getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getBreifComplain() {
		return breifComplain;
	}

	public void setBreifComplain(String breifComplain) {
		this.breifComplain = breifComplain;
	}

	public String getAppintmentTime() {
		return appintmentTime;
	}

	public void setAppintmentTime(String appintmentTime) {
		this.appintmentTime = appintmentTime;
	}

}
