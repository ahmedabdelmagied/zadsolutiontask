package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.domain.Appointment;
import com.example.demo.domain.Doctor;

public interface AppointmentRepo extends JpaRepository<Appointment, Integer> {
	List<Optional<Appointment>> findAllByDoctor(Doctor doctor);
}
