package com.example.demo.dao;
 
import org.springframework.data.jpa.repository.JpaRepository;
 

import com.example.demo.domain.WorkTime;

public interface WorkTimeRepo extends JpaRepository<WorkTime, Integer> {
 
}
