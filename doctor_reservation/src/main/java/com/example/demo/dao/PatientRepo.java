package com.example.demo.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.domain.Patient;

public interface PatientRepo extends JpaRepository<Patient, Integer> {
	@Transactional
	@Modifying
	@Query(value = "UPDATE patient u set doctor_id =:doctor where u.patient_id = :patientId", nativeQuery = true)
	void updatePatient(@Param("doctor") int doctorId, @Param("patientId") int patientId);

	@Transactional
	@Modifying
	@Query(value = "UPDATE patient u set appointment_id =:appointmentId where u.patient_id = :patientId", nativeQuery = true)
	void updatePatientAppointment(@Param("appointmentId") int appointmentId, @Param("patientId") int patientId);

}
