package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

 
import com.example.demo.domain.Doctor;

public interface DoctorRepo extends JpaRepository<Doctor, Integer> {
	List<Optional<Doctor>> findAllBySpecialize(String Specialize);
	 
}
