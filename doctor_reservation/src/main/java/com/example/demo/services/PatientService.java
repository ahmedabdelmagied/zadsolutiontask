package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PatientRepo;
import com.example.demo.domain.Patient;

@Service
public class PatientService {

	@Autowired
	PatientRepo patientRepo;
	
	public void save(Patient patient) {
		patientRepo.save(patient);
	}
}
