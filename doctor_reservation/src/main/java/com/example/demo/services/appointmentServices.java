package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AppointmentRepo;
import com.example.demo.dao.DoctorRepo;
import com.example.demo.dao.PatientRepo;
import com.example.demo.domain.Appointment;
import com.example.demo.domain.Doctor;
import com.example.demo.domain.Patient;

@Service
public class appointmentServices {
	@Autowired
	AppointmentRepo appointmentRepo;
	@Autowired
	DoctorRepo doctorRepo;
	@Autowired
	DoctorService doctorService;
	@Autowired
	PatientService patientService;
	@Autowired
	PatientRepo patientRepo;

	
	public List<Appointment> getAllAppointmentForSpecificDoctor(int doctorId) {
		Doctor doctor = doctorService.getSpecificDoctor(doctorId);
		List<Appointment> allAppointment = new ArrayList<>();
		List<Optional<Appointment>> appointments = appointmentRepo.findAllByDoctor(doctor);
		for (int i = 0; i < appointments.size(); i++) {
			allAppointment.add(appointments.get(i).orElse(null));
		}

		return allAppointment;
	}

	public void saveNewAppointment(Patient patient, int doctorId, Appointment appointment) {
		Doctor doctor = doctorRepo.findById(doctorId).orElse(null);
		patient.setDoctor(doctor);
		patientService.save(patient);
		appointment.setDoctor(doctor);
		appointment.setPatient(patient);
		appointmentRepo.save(appointment);
		patientRepo.updatePatientAppointment(appointment.getAppointmentId(), patient.getPatientId());

	}
	

	public List<Appointment> getAllAppointments() {
		// TODO Auto-generated method stub
		return appointmentRepo.findAll();
	}

}
