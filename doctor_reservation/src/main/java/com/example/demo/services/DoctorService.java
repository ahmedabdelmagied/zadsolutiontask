package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.DoctorRepo;
import com.example.demo.dao.PatientRepo;
import com.example.demo.domain.Doctor;
 
@Service
public class DoctorService {

	@Autowired
	DoctorRepo doctorRepo;

	@Autowired
	PatientRepo patientRepo;

	public Doctor getSpecificDoctor(int doctorId) {
		return doctorRepo.findById(doctorId).orElse(null);
	}

	public void assignDoctorToPatient(int doctorId, int patientId) {
		patientRepo.updatePatient(doctorId, patientId);
	}

	public List<Doctor> getAllDoctors() {
		List<Doctor> allDoctors = doctorRepo.findAll();
		return allDoctors;
	}

	public List<Doctor> getAllDoctorForSpecificSpecialize(String specialize) {
		List<Optional<Doctor>> allDoctors = doctorRepo.findAllBySpecialize(specialize);
		List<Doctor> doctors = new ArrayList<>();
		for (int i = 0; i < allDoctors.size(); i++) {
			doctors.add(allDoctors.get(i).orElse(null));
		}
		return doctors;
	}
	public List<Doctor> getAllDoctorsWithSpecificSpecialize(String specialize) {
		List<Doctor> allDoctors = new ArrayList<>();
		List<Optional<Doctor>> doctors = doctorRepo.findAllBySpecialize(specialize);
		for (int i = 0; i < doctors.size(); i++) {
			allDoctors.add(doctors.get(i).orElse(null));
		}
		return allDoctors;
	}

	public void saveDoctor(Doctor doctor) {
		doctorRepo.save(doctor);
	}
}
