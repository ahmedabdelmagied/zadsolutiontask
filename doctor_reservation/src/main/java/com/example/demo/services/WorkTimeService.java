package com.example.demo.services;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.WorkTimeRepo;
import com.example.demo.domain.WorkTime;

@Service
public class WorkTimeService {

	@Autowired
	 WorkTimeRepo workTimeRepo;
	
	public List<WorkTime> findAllWorkTimeForSpecificDoctor(int doctorId){
		List<Integer> ids = new ArrayList<>();
		ids.add(doctorId);
		List<WorkTime> worktimes = workTimeRepo.findAllById(ids);
		return worktimes;
	}
}
