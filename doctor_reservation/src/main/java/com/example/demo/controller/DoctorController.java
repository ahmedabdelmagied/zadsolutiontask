package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.domain.Appointment;
import com.example.demo.domain.Doctor;
import com.example.demo.services.DoctorService;
import com.example.demo.services.appointmentServices;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	DoctorService doctorService;
	@Autowired
	appointmentServices appService;

	@RequestMapping("/")
	public String index(Model model) {
		List<Doctor> doctors = doctorService.getAllDoctors();
		model.addAttribute("doctors", doctors);
		return "doctors";
	}

	@RequestMapping("/{specialize}")
	public String getDoctorsOfSpecificSpecialize(@PathVariable("specialize") String specialize, Model model) {
		List<Doctor> doctors = doctorService.getAllDoctorForSpecificSpecialize(specialize);
		System.out.println("specialize size = " + doctors.size());
		System.out.println("specialize name = " + specialize);
		model.addAttribute("doctors", doctors);
		return "doctors";
	}

	@RequestMapping(value = "/profile/{doctorId}", method = RequestMethod.GET)
	public String addDoctor(Model model, @PathVariable("doctorId") int doctorId) {
		Doctor doctor = doctorService.getSpecificDoctor(doctorId);
		List<Appointment> appointments = appService.getAllAppointmentForSpecificDoctor(doctorId);
		model.addAttribute("doctor", doctor);
		model.addAttribute("appointments", appointments);
		return "doctorProfile";
	}

}
