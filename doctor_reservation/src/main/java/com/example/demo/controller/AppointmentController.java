package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
import com.example.demo.domain.Appointment;
import com.example.demo.domain.Doctor;
import com.example.demo.domain.Patient;
import com.example.demo.domain.WorkTime;
import com.example.demo.services.DoctorService;
import com.example.demo.services.PatientService;
import com.example.demo.services.WorkTimeService;
import com.example.demo.services.appointmentServices;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

	@Autowired
	appointmentServices appService;
	@Autowired
	PatientService patientService;
	@Autowired
	DoctorService doctorService;
	@Autowired
	WorkTimeService workTimeService;
	Patient patient;
	int doctorId;

	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("patient", new Patient());
		model.addAttribute("specialize", "");
		return "book_appointment/personalInfo";
	}

	@RequestMapping(value = "/doctorInfo", method = RequestMethod.POST)
	public String doctorInfo(@Valid @ModelAttribute("patient") Patient patient,Errors errors,
			@ModelAttribute("specialize") String specialize, Model model) {
	
		if(errors.hasErrors()) {
			 return "book_appointment/personalInfo";
		}
		this.patient = patient;
		List<Doctor> doctors = doctorService.getAllDoctorsWithSpecificSpecialize(specialize);
		model.addAttribute("doctors", doctors);
		model.addAttribute("doctorId", 0);
		return "book_appointment/doctorInfo";
	}

	@RequestMapping("/appointmentInfo")
	public String appointmentInfo(@Valid @ModelAttribute("doctorId") int doctorId,Errors errors, Model model) {
		if(errors.hasErrors())
	          return "book_appointment/doctorInfo";	
		List<WorkTime> workTimes = workTimeService.findAllWorkTimeForSpecificDoctor(doctorId);
		model.addAttribute("worktimes", workTimes);
		model.addAttribute("appointment", new Appointment());
		this.doctorId = doctorId;

		return "book_appointment/appointmentInfo";
	}

	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public String confirmAppointment(@Valid @ModelAttribute("appointment") Appointment appointment, Errors errors , Model model) {
		if(errors.hasErrors())
			return "book_appointment/appointmentInfo";
		appService.saveNewAppointment(this.patient, this.doctorId, appointment);
		return "redirect:/appointment/all";
	}

	@RequestMapping("/all")
	public String showAllAppointments(Model model) {
		List<Appointment> appointments = appService.getAllAppointments();
		model.addAttribute("appointments", appointments);
		return "all_appointments";
	}

}
