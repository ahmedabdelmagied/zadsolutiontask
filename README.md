# Doctor Reversation **Zad Solution Task**

Doctor Reservation is a simple task for managing  doctors and patient and finding an easy way for the patient to find doctor, attach an appointment and Booking doctor

---
### git of the app
---
<img src="https://user-images.githubusercontent.com/60213173/75902253-68600180-5e48-11ea-8b41-5646afb93664.gif">


<p align="center">
<img src="https://user-images.githubusercontent.com/60213173/75889236-46a94f00-5e35-11ea-8f61-1711121e0271.png" width="400" height="550">
<p>



### Database Schema
---
<p align="center">
<img src="https://user-images.githubusercontent.com/60213173/75887807-06e16800-5e33-11ea-8f7c-78936f1e9925.png" width="600" height="450">
</p>


# Note I add some dummy data to database
### how it works?
---

##### After clonning the project in your workspace go to application.properties file and update the following lines
   + spring.datasource.username : "your mysql username"
   + spring.datasource.password : "your mysql password"

---

 
